#!/bin/sh

########################################################################################################################
# Template to wl command - onsole script tool for fast acces to AP/STA info
########################################################################################################################

RWCONFIGS=/etc/Wireless/rf.bin

usage() {
  echo "Usage: $0 | showstat | showcalib | setmaxpwr " >&2
  echo "_____________________________________________________________________________________________________________________" >&2
  echo " " >&2
  echo " wl scan ra0 - scan and show result of wireless interface ra0" >&2
  echo " wl showstat  ra0 - show statistic of wireless interface ra0" >&2
  echo " wl showcalib ra0 - show fabric calibrations for wireless interface ra0" >&2
  echo " wl setmaxpwr ra0 0E0E 0E0E 0E0E 0E0E 0E0E 0E0E 0E0E  - set maximum power 0E for wireless interface ra0, default 0C" >&2
  echo " wl setbasetxpower ra0 AAAA - set base power AA for wireless interface ra0, default 6666" >&2
  echo " wl flashdefaults - write default calirations to flash." >&2
  echo "_____________________________________________________________________________________________________________________" >&2
 exit 1
}

scan()
{
    iwpriv $1 set SiteSurvey=1
    sleep 3
    iwpriv $1 get_site_survey
}

showstat()
{
    if [ "$1" = "" ]; then
	echo "Need iface name as argument."
	exit 1
    fi
    iwpriv $1 show stainfo
    iwpriv $1 show stacountinfo
    iwpriv $1 show stasecinfo
    iwpriv $1 show descinfo
    iwpriv $1 show wdsinfo
    iwpriv $1 show bainfo
    iwpriv $1 show igmpinfo
}

showcalib()
{
    if [ "$1" = "" ]; then
	echo "Need iface name as argument."
	exit 1
    fi
    iwpriv $1 e2p
}

setmaxpwr()
{
    if [ "$8" = "" ]; then
	echo "Need 8 args. Fisrt - wifi interface, others power of two channels."
	echo "example wl setmaxpwr ra0 0E0E 0E0E 0E0E 0E0E 0E0E 0E0E 0E0E"
	exit 1
    fi

    iwpriv $1 e2p 52=$2
    iwpriv $1 e2p 54=$3
    iwpriv $1 e2p 56=$4
    iwpriv $1 e2p 58=$5
    iwpriv $1 e2p 5A=$6
    iwpriv $1 e2p 5C=$7
    iwpriv $1 e2p 5E=$8

    echo "After set - need reboot."
}

setbasetxpower()
{
    if [ "$2" = "" ]; then
	echo "Need iface name as argument and value."
	exit 1
    fi
    iwpriv $1 e2p DE=$2
    iwpriv $1 e2p E0=$2
    iwpriv $1 e2p E2=$2
    iwpriv $1 e2p E4=$2
    iwpriv $1 e2p E6=$2
    iwpriv $1 e2p E8=$2
    iwpriv $1 e2p EA=$2
    iwpriv $1 e2p EC=$2
    iwpriv $1 e2p EE=$2

    echo "After set - need reboot."
}

flashdefaults()
{
    echo "Erase wifi Factory part and write defaults"
    mtd_write erase Factory
    mtd_write write $RWCONFIGS Factory
    sync

    echo "After write - need reboot."
}

main() {
  local cmd="$1"
  [ -z "$cmd" ] && usage
  shift
  case "$cmd" in
    scan)
      scan $@
      ;;
    showstat)
      showstat $@
      ;;
    showcalib)
      showcalib $@
      ;;
    setmaxpwr)
      setmaxpwr $@
      ;;
    setbasetxpower)
      setbasetxpower $@
      ;;
    flashdefaults)
      flashdefaults $@
      ;;
    *)
      usage $@
      ;;
  esac
}

main $@
