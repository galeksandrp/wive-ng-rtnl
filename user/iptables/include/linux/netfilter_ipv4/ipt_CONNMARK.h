/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */
#ifndef _IPT_CONNMARK_H_target
#define _IPT_CONNMARK_H_target

/* Copyright (C) 2002,2004 MARA Systems AB <http://www.marasystems.com>
 * by Henrik Nordstrom <hno@marasystems.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

enum {
	IPT_CONNMARK_SET = 0,
	IPT_CONNMARK_SAVE,
	IPT_CONNMARK_RESTORE
};

struct ipt_connmark_target_info {
#ifdef KERNEL_64_USERSPACE_32
	unsigned long long mark;
	unsigned long long mask;
#else
	unsigned long mark;
	unsigned long mask;
#endif
	u_int8_t mode;
};

#endif /*_IPT_CONNMARK_H_target*/
